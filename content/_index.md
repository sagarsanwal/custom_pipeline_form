## Select a Feature Model to view

  <div id="pipeline-form">
   <form action="#" method="post">
     <select id="projectid" style = "width: -webkit-fill-available; border-radius: 5px; border-color: blue; margin: 2.5px" >
      <option selected>Select a Project Name</option>
      <option>Apple Pie</option>
      <option>Key Lime</option>
      <option>Elclair</option>
      <option>Cheesecake</option>
      <option>Banana Pudding</option>
     </select>
     </br>
     <select id="folderid" style = "width: -webkit-fill-available; border-radius: 5px; border-color: blue;  margin: 2.5px" >
      <option selected>Select a Folder Name</option>
      <option>Apple Pie Folder</option>
      <option>Key Lime Folder</option>
      <option>Eclair Folder</option>
      <option>Cheesecake Folder</option>
      <option>Banana Pudding Folder</option>
     </select>
     </br></br>
     <button type="button" id="api-example">Open</button>
   </form>
  </div>
  <div id="pipeline-after" style="display:none;">
   <p id="api-output">Please stand by .. </p>
  </div>
